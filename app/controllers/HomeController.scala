package controllers

import scala.concurrent.Future

import javax.inject.{ Inject, Singleton }
import models.{ Event, WithCors }
import play.api.libs.json.{ JsValue, Json }
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import play.api.libs.ws.{ WSClient, WSResponse }
import play.api.mvc.{ Action, Controller }
import play.api.routing.JavaScriptReverseRouter

@Singleton
class HomeController @Inject() (ws: WSClient) extends Controller {

  def index = WithCors("GET", "POST") {
    Action {
      Ok(views.html.index("Your new application is ready."))
    }
  }

  def dashboard = WithCors("GET", "POST") {
    Action {
      Ok(views.html.dashboard("Dashboard"));
    }
  }

  def openAccount() = WithCors("GET", "POST") {
    Action { implicit request =>
      val futureResponse: Future[WSResponse] = ws.url("http://localhost:8080/open/account").get();

      Redirect(routes.HomeController.index());
    }
  }

  def closeAccount(accountId: String) = WithCors("GET", "POST") {
    Action { implicit request =>
      val futureResponse: Future[WSResponse] = ws.url("http://localhost:8080/close/account").withQueryString(
        "accountId" -> accountId).get();

      Redirect(routes.HomeController.index());
    }
  }

  def changeAccountStatus(accountId: String, status: String) = WithCors("GET", "POST") {
    Action { implicit request =>
      val futureResponse: Future[WSResponse] = ws.url("http://localhost:8080/change/status").withQueryString(
        "accountId" -> accountId, "status" -> status).get();

      Redirect(routes.HomeController.index());
    }
  }

  def deposit(accountId: String, amount: String, currency: String) = WithCors("GET", "POST") {
    Action { implicit request =>
      val futureResponse: Future[WSResponse] = ws.url("http://localhost:8080/account/deposit").withQueryString(
        "accountId" -> accountId, "depositAmount" -> amount, "currencyCode" -> currency).get();

      Redirect(routes.HomeController.index());
    }
  }

  def withdraw(accountId: String, amount: String, currency: String) = WithCors("GET", "POST") {
    Action { implicit request =>
      val futureResponse: Future[WSResponse] = ws.url("http://localhost:8080/account/withdraw").withQueryString(
        "accountId" -> accountId, "withdrawAmount" -> amount, "currencyCode" -> currency).get();

      Redirect(routes.HomeController.index());
    }
  }

  def fetchEvents() = WithCors("GET", "POST") {
    Action { implicit request =>

      var events = List[Event]();

      implicit val context = play.api.libs.concurrent.Execution.Implicits.defaultContext

      ws.url("http://localhost:8080/fetch/account/events").get().map { respones =>

        val json: JsValue = Json.parse(respones.json.toString())
        events = json.as[List[Event]];
      }

      Thread.sleep(5000)

      Ok(Json.obj("events" -> events));
    }
  }

  def javascriptRoutes() = Action { implicit request =>
    Ok(JavaScriptReverseRouter("jsRoutes")(
      routes.javascript.HomeController.fetchEvents,
      routes.javascript.HomeController.deposit,
      routes.javascript.HomeController.withdraw,
      routes.javascript.HomeController.openAccount,
      routes.javascript.HomeController.closeAccount)).as("text/javascript")
  }
}
