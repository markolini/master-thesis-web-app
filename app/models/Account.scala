package models

import play.api.libs.json.Json

case class CloseAccount(accountId: String)
case class AccountStatusChange(accountId: String, status: String)
case class Deposit(accountId: String, amount: String, currency: String)
case class Withdraw(accountId: String, amount: String, currency: String)

object CloseAccount {

  implicit val closeFormat = Json.format[CloseAccount];
}

object AccountStatusChange {

  implicit val statusFormat = Json.format[AccountStatusChange];
}

object Deposit {

  implicit val depositFormat = Json.format[Deposit];

}

object Withdraw {

  implicit val withdrawFormat = Json.format[Withdraw];
}
