package models

import play.api.libs.json._

case class Event(name: String, accountId: String, amount: String, status: String) {

  def this() = this("", "", "", "")
}

object Event {
  implicit object EventFormat extends Format[Event] {

    def reads(json: JsValue) = JsSuccess(Event(
      (json \ "name").as[String],
      (json \ "accountId").as[String],
      (json \ "amount").as[String],
      (json \ "status").as[String]))

    def writes(event: Event) = JsObject(Seq(
      "name" -> JsString(event.name),
      "accountId" -> JsString(event.accountId),
      "amount" -> JsString(event.amount),
      "status" -> JsString(event.status)))
  }
}
