$("#open").click(function() {
	openAccount();
});

$("#fetch").click(function() {
	fetchEvents();
});


$("#close").click(function() {
	var accountId = document.getElementById("closeAccountId").value
	closeAccount(accountId);
});

$("#deposit").click(function() {
	var accountId = document.getElementById("depositAccountId").value
	var amount = document.getElementById("depositAmount").value
	var currency = document.getElementById("depositCurrency").value
	deposit(accountId, amount, currency);
});

$("#withdraw").click(function() {
	var accountId = document.getElementById("withdrawAccountId").value
	var amount = document.getElementById("withdrawAmount").value
	var currency = document.getElementById("withdrawCurrency").value
	withdraw(accountId, amount, currency);
});

var withdraw = function(accountId, amount, currency) {

	var r = jsRoutes.controllers.HomeController.withdraw(accountId, amount, currency);

	$.ajax({
		url : r.url,
		type : 'GET',
		data : {
			accountId : accountId,
			amount : amount,
			currency : currency
		},
		success : function(data) {
			alert("Withdrawn")
		}
	});
};

var openAccount = function() {

	var r = jsRoutes.controllers.HomeController.openAccount();

	$.ajax({
		url : r.url,
		type : 'GET',
		success : function(data) {
			alert("Opened")
		}
	});
};

var closeAccount = function(accountId) {

	var r = jsRoutes.controllers.HomeController.closeAccount(accountId);
	
	$.ajax({
		url : r.url,
		type : 'GET',
		success : function(data) {
			alert("Closed")
		}
	});
};

var deposit = function(accountId, amount, currency) {

	var r = jsRoutes.controllers.HomeController.deposit(accountId, amount, currency);

	$.ajax({
		url : r.url,
		type : 'GET',
		data : {
			accountId : accountId,
			amount : amount,
			currency : currency
		},
		success : function(data) {
			alert("Deposited")
		}
	});
};

var fetchEvents = function() {

	var r = jsRoutes.controllers.HomeController.fetchEvents();

	$.ajax({
		url : r.url,
		type : 'GET',
		success : function(data) {
			events = data.events;
			drawTable(events);
		}
	});

	function drawTable(events) {
		createColumns();
		for (var i = 0; i < events.length; i++) {
			drawRow(events[i]);
		}
	}

	function drawRow(rowData) {
		var row = $("<tr />")
		$("#accounts").append(row);
		row.append($("<td>" + rowData.name + "</td>"));
		row.append($("<td>" + rowData.accountId + "</td>"));
		row.append($("<td>" + rowData.amount + "</td>"));
		row.append($("<td>" + rowData.status + "</td>"));
	}

	function createColumns() {
		$("#accounts").empty();
		var row = $("<tr />")
		$("#accounts").append(row);
		row.append($("<th data-field='name'>" + "Name" + "</th>"));
		row.append($("<th data-field='accountId'>" + "Account ID" + "</th>"));
		row.append($("<th data-field='amount'>" + "Amount and Currency"
				+ "</th>"));
		row.append($("<th data-field='status'>" + "Status" + "</th>"));
	}
};
